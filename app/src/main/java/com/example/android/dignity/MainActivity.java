package com.example.android.dignity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class MainActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {
    private DatabaseReference mDatabaseReference;
    private String count;
    private String buzzer;
    private ImageView happy,average,sad;
    private long incentives;
    private static final String station_name = "EBS";
    private int totalCount=0;
    private long initialIncentive;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private String mUsername;
    private FirebaseUser mFirebaseUser;
    private FirebaseAuth mFirebaseAuth;
    private GoogleApiClient mGoogleApiClient;
    private String mPhotoUrl;
    private double currentRating;
    private double rating;
    private TextView incentivesTextView;
    private SharedPreferences pref;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        happy= (ImageView) findViewById(R.id.happy);
        sad= (ImageView) findViewById(R.id.sad);
        average= (ImageView) findViewById(R.id.average);
        incentivesTextView= (TextView) findViewById(R.id.incentives);
        mUsername = "ANONYMOUS";
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        initialIncentive=incentives= pref.getLong("incentiveData",0);
        if (mFirebaseUser == null) {
            // Not signed in, launch the Sign In activity
            startActivity(new Intent(this, SignInActivity.class));
            finish();
            return;
        } else {
            mUsername = mFirebaseUser.getDisplayName();
            if (mFirebaseUser.getPhotoUrl() != null) {
                mPhotoUrl = mFirebaseUser.getPhotoUrl().toString();
            }
        }
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API)
                .build();

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                } else {
                }
            }
        };
        happy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (incentives < 13000)
                        incentives += 6;
                    storeData(incentives);
                    totalCount++;
                    infinite();
                    databaseChange();

            }
        });
        average.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                    totalCount++;
                    if (incentives < 13000)
                        incentives += 2;
                    storeData(incentives);
                    infinite();
                    databaseChange();

            }
        });
        sad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    totalCount++;
                    if (incentives < 9000)
                        incentives -= 8;
                    else incentives -= 10;
                    storeData(incentives);
                    infinite();
                    databaseChange();

            }
        });

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
    public void storeData(long incentive){
        SharedPreferences.Editor editor = pref.edit();
        editor.putLong("incentiveData", incentive);
        editor.commit();
        editor.apply();

    }
    public void databaseChange() {
        if (totalCount % 5 == 0) {
            rating = (double)(incentives - initialIncentive) / 5.0;
            initialIncentive = incentives;
            currentRating = rating;
        }
        long incentive = pref.getLong("incentiveData",1000);
        mDatabaseReference.child("messages").child(mUsername).child("rate").setValue(currentRating);
        mDatabaseReference.child("messages").child(mUsername).child("incentives").setValue(incentive);
        mDatabaseReference.child("messages").child(mUsername).child("name").setValue(mUsername);
        mDatabaseReference.child("messages").child(mUsername).child("photoUrl").setValue(mPhotoUrl);
        incentivesTextView.setText(incentive + "");


    }
    public void infinite() {
        mDatabaseReference= FirebaseDatabase.getInstance().getReference();
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        String url = "http://192.168.43.160";
        final StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                String s = response.toString();
                int ind1 = s.lastIndexOf('#');
                int len = s.length();
                count = s.substring(1, ind1);
                buzzer = s.substring(ind1 + 1, len);
                Toast.makeText(MainActivity.this, "COUNT-> "+count+" BUZZER->"+buzzer, Toast.LENGTH_SHORT).show();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(MainActivity.this, "Didn't work", Toast.LENGTH_SHORT).show();
            }
        });
        queue.add(stringRequest);
    }







}
